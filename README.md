## Synopsis

This is demo code for multilevel Monte Carlo (MLMC) computations
in MATLAB.

For mathematical details, see 

    @article{giles2015,
    author = {Giles, M. B.},
    title = {Multilevel {Monte Carlo} methods},
    journal = {Acta Numerica},
    volume = {24},
    month = {5},
    year = {2015},
    pages = {259--328},
    doi = {10.1017/S096249291500001X},
    }

For more information on MLMC software [click here](https://people.maths.ox.ac.uk/~gilesm/mlmc/).

## Authors

M. B. Giles   <mike.giles@maths.ox.ac.uk>

## Installation

To download:

    $ git clone https://bitbucket.org/CasperBeentjes/mlmc-matlab.git   
    $ cd mlmc-matlab

To add MLMC-core routines via command line:

    $ export MATLABPATH=/path/to/mlmc-matlab/mlmc-core:$MATLABPATH

To add MLMC-core routines via MATLAB IDE:

    $ addpath(/path/to/mlmc-matlab/mlmc-core)

To make this permanent consider [adding this line to your startup.m file](mathworks.com/help/matlab/matlab_env/add-folders-to-matlab-search-path-at-startup.html).

## MLMC-core routines

The [/mlmc-core directory](https://bitbucket.org/CasperBeentjes/mlmc-matlab/src/master/mlmc-core/) contains common routines used by MLMC applications, such as driver, test and plotting routines.

## Examples

Many examples illustrating the use of the MLMC method using the MLMC-core MATLAB routines are included in the [/examples directory](https://bitbucket.org/CasperBeentjes/mlmc-matlab/src/master/examples/).

## Licensing and acknowledgements

This code is freely available to all under a GPL license -- anyone requiring a more permissive license for commercial purposes should contact M. B. Giles.

The software is based on the research reported in the papers listed [here](https://people.maths.ox.ac.uk/~gilesm/mlmc.html). If you find it helpful in your research, the papers there can be cited in any publications. I would also be interested to hear about it, particularly if it is used for novel applications.

The underlying research has been supported by

* EPSRC, through a Springboard Fellowship and project funding
* Oxford-Man Institute of Quantitative Finance
