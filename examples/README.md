## Examples

This folder contains various example applications. Folder for generic application _app_ typically contains:

* _app_.m
    * main application code (running simulations + generating plots)
* _app_\_figures.m
    * plotting script for standalone rendering of figures based on simulation output
* _app_.tex
    * LaTeX file for output
* Makefile
    * for easy creation of data (make _app_), figures (make figures) and PDF (make pdf) output

### OPRE

Financial options based on scalar geometric Brownian motion and Heston models, similar to original [2008 Operations Research paper](https://people.maths.ox.ac.uk/~gilesm/files/OPRE_2008.pdf), using an Euler-Maruyama discretisation.

    $ cd mlmc-matlab/examples/opre
    $ matlab -nodisplay -nosplash -r "opre; exit"

### MCQMC06

Financial options based on scalar geometric Brownian motion, similar to [MCQMC06 paper](https://people.maths.ox.ac.uk/~gilesm/files/mcqmc06.pdf), using a Milstein discretisation.

    $ cd mlmc-matlab/examples/mcqmc06

Standard version

    $ matlab -nodisplay -nosplash -r "mcqmc06; exit"

Version using [SPMD parallelism in MATLAB](https://mathworks.com/help/parallel-computing/spmd.html).

    $ matlab -nodisplay -nosplash -r "mcqmc06_spmd; exit"

### Basket options

Basket options based on 5 underlying assets, similar to [2009 Winter Simulation Conference paper](https://people.maths.ox.ac.uk/~gilesm/files/wsc09.pdf), using a Milstein discretisation.

    $ cd mlmc-matlab/examples/basket
    $ matlab -nodisplay -nosplash -r "basket; exit"

### Reflected diffusion 

1D reflected diffusions, giving results for a [conference presentation](https://people.maths.ox.ac.uk/~gilesm/talks/SciCADE_15.pdf) at SciCADE 2015.

    $ cd mlmc-matlab/examples/reflected
    $ matlab -nodisplay -nosplash -r "reflected; exit"

