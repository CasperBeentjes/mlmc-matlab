%
% execute MATLAB code in IDE to generate data
% or run:
%
% matlab -r -nodisplay basket
% 
%
% use this MATLAB routine to plot output
%

% Add MLMC core routines
addpath('../../mlmc-core/');

nvert = 2;

% Specify location of MLMC test data
data_dir   = './data/';

% Specify directory to save figures in
figure_dir = './figures/';
% Create figure directory if it doesn't exist
[~,~,~] = mkdir(figure_dir);

for option = 1:5
  filename = ['basket_' num2str(option)];
  mlmc_plot([data_dir filename], nvert);

  subplot(nvert,2,2*nvert-1)
  current = axis; axis([current(1:2) 10 current(4)])

  if option == 4
    subplot(nvert,2,1)
    current = axis; axis([current(1:2) -10 15])
    subplot(nvert,2,2*nvert-1)
    current = axis; axis([current(1:2) 100 1e8])
    subplot(nvert,2,2*nvert)
    current = axis; axis([current(1:2) 100 1e7])
  end

  if (nvert == 1)
    figure(1)
    print('-deps2',[figure_dir filename 'a.eps'])
    figure(2)
    print('-deps2',[figure_dir filename 'b.eps'])
  else
    print('-deps2',[figure_dir filename '.eps'])
  end
end
