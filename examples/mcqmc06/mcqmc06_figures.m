%
% execute MATLAB code in IDE to generate data
% or run:
%
% matlab -r -nodisplay mcqmc06
% matlab -r -nodisplay mcqmc06_spmc
% 
%
% use this MATLAB routine to plot output

% Add MLMC core routines
addpath('../../mlmc-core/');

nvert = 3;

% Specify location of MLMC test data
data_dir   = './data/';

% Specify directory to save figures in
figure_dir = './figures/';
% Create figure directory if it doesn't exist
[~,~,~] = mkdir(figure_dir);

% Generate figures for mcqmc06.m
for option = 1:5
  filename = ['mcqmc06_' num2str(option)];
  mlmc_plot([data_dir filename], nvert);

  if (nvert==1)
    figure(1)
    print('-deps2c',[figure_dir filename '_a.eps'])
    figure(2)
    print('-deps2c',[figure_dir filename '_b.eps'])
  else
    print('-deps2c',[figure_dir filename '.eps'])
  end

  filename = strcat(filename, '_100');
  mlmc_plot_100([data_dir filename]);
  print('-deps2c',[figure_dir filename '.eps'])
end

% Generate figures for mcqmc06_spmd.m
for option = 1:5
  filename = ['mcqmc06_spmd_' num2str(option)];
  mlmc_plot([data_dir filename], nvert);

  if (nvert==1)
    figure(1)
    print('-deps2c',[figure_dir filename '_a.eps'])
    figure(2)
    print('-deps2c',[figure_dir filename '_b.eps'])
  else
    print('-deps2c',[figure_dir filename '.eps'])
  end

end
