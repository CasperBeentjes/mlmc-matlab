%
% These are similar to the MLMC tests for the original
% 2008 Operations Research paper, using an Euler-Maruyama
% discretisation with 4^l timesteps on level l.
%
% The differences are:
% -- the plots do not have the extrapolation results
% -- the top two plots are log_2 rather than log_4
% -- the new MLMC driver is a little different
% -- switch to X_0=100 instead of X_0=1
%

function opre

close all; clear all;

% Import MLMC core routines
addpath('../../mlmc-core/')

% Specify data directory 
data_dir = './data/';
% Create data directory if it doesn't exist
[~,~,~] = mkdir(data_dir);

% Specify figure directory
figure_dir = './figures/';
% Create figure directory if it doesn't exist
[~,~,~] = mkdir(figure_dir);

% GBM model parameters
r      = 0.05;    % drift
sig    = 0.2;     % local volatility
K      = 100;     % strike price
S0     = 100;     % initial value stock price
T      = 1;       % time interval
% Heston model additional parameters
lambda = 5;       % rate of reversal of instantateous variance
xsi    = 0.25;    % volatility of volatility
V0     = 0.04;    % initial value instantaneous variance
rho    = -0.5;    % correlation between dW_S and dW_V
% Create parameter struct to pass to mlmc_l
params = struct('r',r, 'sig',sig, 'K',K, 'S0', S0, 'T',T, ...
    'lambda',lambda, 'xsi',xsi, 'V0',V0, 'rho',rho);

% MLMC parameters
N0    = 1000;   % initial samples on coarse levels
Lmin  = 2;      % minimum refinement level
Lmax  = 6;      % maximum refinement level

for option = 1:5
  if (option == 1)
    fprintf(1, '\n ---- European call ---- \n');
    N      = 2000000;       % samples for convergence tests
    L      = 5;             % levels for convergence tests
    Eps    = [ 0.005 0.01 0.02 0.05 0.1 ];
  elseif (option == 2)
    fprintf(1, '\n ---- Asian call ---- \n');
    N      = 2000000;  % samples for convergence tests
    L      = 5;        % levels for convergence tests
    Eps    = [ 0.005 0.01 0.02 0.05 0.1 ];
  elseif (option == 3)
    fprintf(1, '\n ---- lookback call ---- \n');
    N      = 2000000;  % samples for convergence tests
    L      = 5;        % levels for convergence tests
    Eps    = [ 0.01 0.02 0.05 0.1 0.2 ];
  elseif (option == 4)
    fprintf(1, '\n ---- digital call ---- \n');
    N      = 3000000;  % samples for convergence tests
    L      = 5;        % levels for convergence tests
    Eps    = [ 0.02 0.05 0.1 0.2 0.5 ];
  elseif (option == 5)
    fprintf(1, '\n ---- Heston model ---- \n');
    N      = 2000000;  % samples for convergence tests
    L      = 5;        % levels for convergence tests
    Eps    = [ 0.005 0.01 0.02 0.05 0.1 ];
  end

  if (option < 5)
    filename = ['opre_gbm' num2str(option)];
  else
    filename = 'opre_heston';
  end
  fp = fopen([data_dir filename '.txt'], 'w');
  mlmc_test(@opre_l, N,L, N0,Eps,Lmin,Lmax, fp, option,params);
  fclose(fp);

%
% plot results
%
  nvert = 3;
  mlmc_plot([data_dir filename], nvert);

  if(nvert == 1)
    figure(1)
    print('-deps2', [figure_dir filename 'a.eps'])
    figure(2)
    print('-deps2', [figure_dir filename 'b.eps'])
  else
    print('-deps2', [figure_dir filename '.eps'])
  end

%
% print exact analytic value
%

  d1  = (log(S0/K) + (r+0.5*sig^2)*T) / (sig*sqrt(T));
  d2  = (log(S0/K) + (r-0.5*sig^2)*T) / (sig*sqrt(T));

  if (option == 1)
    val = S0*ncf(d1) - K*exp(-r*T)*ncf(d2);
    fprintf(1, '\n Exact value: %f \n', val);
  elseif (option == 3)
    k   = 0.5*sig^2/r;
    d1 = (r+0.5*sig^2)*T / (sig*sqrt(T));
    d2 = (r-0.5*sig^2)*T / (sig*sqrt(T));
    val = S0*( ncf(d1) - ncf(-d1)*k - exp(-r*T)*(ncf(d2) - ncf(d2)*k) );
    fprintf(1, '\n Exact value: %f \n', val);
  elseif (option == 4)
    val = K*exp(-r*T)*ncf(d2);
    fprintf(1, ' Exact value: %f \n\n', val);
  else
    val = NaN;
    fprintf(1, '\n Exact value unknown \n\n');
  end

%
% now do 100 MLMC calcs in parallel
%
  filename = strcat(filename, '_100');
  fp = fopen([data_dir filename '.txt'], 'w');
  mlmc_test_100(@opre_l, val, N0,Eps,Lmin,Lmax, fp, option,params);
  fclose(fp);

%
% plot results
%
  mlmc_plot_100([data_dir filename]);
  print('-deps2', [figure_dir filename '.eps'])
end

end


%-------------------------------------------------------
%
% level l estimator for Operations Research paper
%

function [sums, cost] = opre_l(l,N, option,params)

% Extract model parameters
r       = params.r;
sig     = params.sig;
K       = params.K;
S0      = params.S0;
T       = params.T;
lambda  = params.lambda;
xsi     = params.xsi;
V0      = params.V0;
rho     = params.rho;

% Discretisation refinement parameter
M = 4;

nf = M^l;
nc = nf/M;

hf = T/nf;
hc = T/nc;

sums(1:6) = 0;

for N1 = 1:10000:N
  N2 = min(10000, N-N1+1);

%
% GBM model
%
  if option < 5
    X0 = S0;

    Xf = X0*ones(1,N2);
    Xc = Xf;

    Af = 0.5*hf*Xf;
    Ac = 0.5*hc*Xc;

    Mf = Xf;
    Mc = Xc;

    if l == 0
      dWf = sqrt(hf)*randn(1,N2);
      Xf  = Xf + r*Xf*hf + sig*Xf.*dWf;
      Af = Af + 0.5*hf*Xf;
      Mf = min(Mf, Xf);
    else
      for n = 1:nc
        dWc = zeros(1,N2);
        for m = 1:M
          dWf = sqrt(hf)*randn(1,N2);
          dWc = dWc + dWf;
          Xf  = Xf + r*Xf*hf + sig*Xf.*dWf;
          Af  = Af + hf*Xf;
          Mf  = min(Mf, Xf);
        end
        Xc = Xc + r*Xc*hc + sig*Xc.*dWc;
        Ac = Ac + hc*Xc;
        Mc = min(Mc, Xc);
      end
      Af = Af - 0.5*hf*Xf;
      Ac = Ac - 0.5*hc*Xc;
    end

    if option == 1
      Pf = max(0,Xf-K);
      Pc = max(0,Xc-K);
    elseif option == 2
      Pf = max(0,Af-K);
      Pc = max(0,Ac-K);
    elseif option == 3
      beta = 0.5826;  % special factor for offset correction
      Pf = Xf - Mf*(1-beta*sig*sqrt(hf));
      Pc = Xc - Mc*(1-beta*sig*sqrt(hc));
    elseif option == 4
      Pf = K * 0.5*(sign(Xf-K)+1);
      Pc = K * 0.5*(sign(Xc-K)+1);
    end
%
% Heston model
%
  else
    X0 = [S0; V0];
    Xf = X0*ones(1,N2);
    Xc = Xf;

    if l == 0
      dWf = sqrt(hf)*randn(2,N2);
      Xf  = Xf + mu(Xf,hf, r,sig,lambda)*hf + ...
                sig_dW(Xf,dWf,hf, lambda,xsi,rho);

    else
      for n = 1:nc
        dWc = zeros(2,N2);
        for m = 1:M
          dWf = sqrt(hf)*randn(2,N2);
          dWc = dWc + dWf;
          Xf  = Xf + mu(Xf,hf, r,sig,lambda)*hf + ...
                sig_dW(Xf,dWf,hf, lambda,xsi,rho);
        end
        Xc = Xc + mu(Xc,hc, r,sig,lambda)*hc + ...
                sig_dW(Xc,dWc,hc, lambda,xsi,rho);
      end
    end

    Pf = max(0,Xf(1,:)-K);
    Pc = max(0,Xc(1,:)-K);
  end

  if l == 0
    Pc = 0;
  end

  dP = exp(-r*T)*(Pf-Pc);
  Pf = exp(-r*T)*Pf;

  sums(1) = sums(1) + sum(dP);
  sums(2) = sums(2) + sum(dP.^2);
  sums(3) = sums(3) + sum(dP.^3);
  sums(4) = sums(4) + sum(dP.^4);
  sums(5) = sums(5) + sum(Pf);
  sums(6) = sums(6) + sum(Pf.^2);
end

cost = N*nf;   % cost defined as number of fine timesteps

end

%-------------------------------------------------------
%
% Helper functions for opre_l and exact value
%

%
% Drift in Heston model
%

function m = mu(x,h, r,sig,lambda)
    % Standard Euler discretisation of Heston model
    %m = [ r*x(1,:); ...
    %       lambda*(sig^2-x(2,:)) ];

    % Improved Euler discretisation of Heston model
    % removing drift from volatility equation
    m = [ r*x(1,:); ...
           ((1-exp(-lambda*h))/h)*(sig^2-x(2,:)) ];
end

%
% Volatility in Heston model
%

function sigdW = sig_dW(x,dW,h, lambda,xsi,rho)
    % Correlated Wiener increments
    dW(2,:) = rho*dW(1,:) + sqrt(1-rho^2)*dW(2,:);

    % Standard Euler discretisation of Heston model
    %sigdW = [ sqrt(max(0,x(2,:))).*x(1,:).*dW(1,:);  ...
    %          xsi*sqrt(max(0,x(2,:))).*dW(2,:) ];

    % Improved Euler discretisation of Heston model
    % removing drift from volatility equation
    sigdW = [ sqrt(max(0,x(2,:))).*x(1,:).*dW(1,:);  ...
              exp(-lambda*h)*xsi*sqrt(max(0,x(2,:))).*dW(2,:) ];
end

%
% Normal CDF function
%

function N = ncf(x)
    N = 0.5*erfc(-x/sqrt(2));
end
