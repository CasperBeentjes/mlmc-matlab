%
% execute MATLAB code in IDE to generate data
% or run:
%
% matlab -r -nodisplay opre
% 
%
% use this MATLAB routine to plot output
%

% Add MLMC core routines
addpath('../../mlmc-core/');

nvert = 3;

% Specify location of MLMC test data
data_dir   = './data/';

% Specify directory to save figures in
figure_dir = './figures/';
% Create figure directory if it doesn't exist
[~,~,~] = mkdir(figure_dir);

for option = 1:5
  if (option<5)
    filename = ['opre_gbm' num2str(option)];
  else
    filename = 'opre_heston';
  end
  mlmc_plot([data_dir filename], nvert);

  if (nvert==1)
    figure(1)
    print('-deps2c',[figure_dir filename '_a.eps'])
    figure(2)
    print('-deps2c',[figure_dir filename '_b.eps'])
  else
    print('-deps2c',[figure_dir filename '.eps'])
  end

  filename = strcat(filename, '_100');
  mlmc_plot_100([data_dir filename]);
  print('-deps2c',[figure_dir filename '.eps'])
end
