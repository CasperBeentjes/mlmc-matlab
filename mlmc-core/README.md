## MLMC core routines

This directory contains common routines used by all applications.

#### mlmc.m

Main MLMC driver routine, estimates expectation using MLMC.

#### mlmc\_test.m

MLMC test routine for convergence (weak error, variance, kurtosis, cost) and complexity.

#### mlmc\_plot.m

Plotting routine for results from mlmc\_test.m routine.

#### mlmc\_test\_100.m

MLMC test routine to calculate 100 independent MLMC estimates.

#### mlmc\_plot\_100.m

Plotting routine for results from mlmc\_test\_100.m routine.
