%
% function mlmc_test_100(mlmc_fn, val, N0,Eps,Lmin,Lmax, fp, varargin)
%
% Multilevel Monte Carlo test routine to perform 100 independent MLMC calculations
% in parallel. Prints results to stdout and file.
%
% Inputs:
%     mlmc_fn = the user low-level routine for level l estimator. Its interface is
%
%        [sums, cost] = mlmc_fn(l,N, varargin)
%
%         Inputs:  l = level
%                  N = number of samples
%                  varargin = optional additional user variables
%
%         Outputs: sums(1) = sum(Y)
%                  sums(2) = sum(Y.^2)
%                  where Y are iid samples with expected value:
%                     E[P_0]           on level 0
%                     E[P_l - P_{l-1}] on level l > 0
%                  cost = user-defined computational cost of N samples
%
% val      = exact value (NaN if not known)
%
% N0       = initial number of samples for MLMC calcs
% Eps      = desired accuracy array for MLMC calcs
% Lmin     = minimum number of levels for MLMC calcs
% Lmax     = maximum number of levels for MLMC calcs
%
% fp       = file handle for printing to file
%
% varargin = optional additional user variables to be passed to mlmc_fn
%

function mlmc_test_100(mlmc_fn, val, N0,Eps,Lmin,Lmax, fp, varargin)

PRINTF2(fp, '\n');
PRINTF2(fp, '**********************************************************\n');
PRINTF2(fp, '*** MLMC_100 file version 0.9     produced by          ***\n');
PRINTF2(fp, '*** MATLAB mlmc_test_100 on %s       ***\n', datestr(now));
PRINTF2(fp, '**********************************************************\n');
PRINTF2(fp, '\n');
PRINTF2(fp, '***************************************** \n');
PRINTF2(fp, '*** MLMC errors from 100 calculations *** \n');
PRINTF2(fp, '***************************************** \n');

if isnan(val)
  PRINTF2(fp, '\n Exact value unknown \n');
else
  PRINTF2(fp, '\n Exact value: %f \n', val);
end

for i = 1:length(Eps)
  PRINTF2(fp, '\n eps = %.3e \n-----------------\n', Eps(i)); 

  P100 = zeros(1,100);
  parfor j=1:100
    RandStream.setGlobalStream( ...
    RandStream.create('mrg32k3a', 'NumStreams', 100, 'StreamIndices', j));

    P100(j) = mlmc(N0,Eps(i),Lmin,Lmax, mlmc_fn,  0,0,0, varargin{:});
  end
  for j=1:5:100
    PRINTF2(fp, ' %.5e ',P100(j:j+4));
    PRINTF2(fp, '\n');
  end
end

end

%
% function to print to both a file and stdout 
%

function PRINTF2(fp,varargin)
    fprintf(fp,varargin{:});
    fprintf( 1,varargin{:});
end
